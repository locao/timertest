﻿namespace TimerTest
{
    partial class TimerTestFormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
            this.labelTimer1 = new System.Windows.Forms.Label();
            this.labelTimer2 = new System.Windows.Forms.Label();
            this.buttonSetTimer1 = new System.Windows.Forms.Button();
            this.buttonSetTimer2 = new System.Windows.Forms.Button();
            this.dateTimePickerTimer1 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerTimer2 = new System.Windows.Forms.DateTimePicker();
            this.listBoxConsole = new System.Windows.Forms.ListBox();
            this.tableLayoutPanelMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanelMain
            // 
            this.tableLayoutPanelMain.ColumnCount = 3;
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 76F));
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 76F));
            this.tableLayoutPanelMain.Controls.Add(this.labelTimer1, 0, 0);
            this.tableLayoutPanelMain.Controls.Add(this.labelTimer2, 0, 1);
            this.tableLayoutPanelMain.Controls.Add(this.buttonSetTimer1, 2, 0);
            this.tableLayoutPanelMain.Controls.Add(this.buttonSetTimer2, 2, 1);
            this.tableLayoutPanelMain.Controls.Add(this.dateTimePickerTimer1, 1, 0);
            this.tableLayoutPanelMain.Controls.Add(this.dateTimePickerTimer2, 1, 1);
            this.tableLayoutPanelMain.Controls.Add(this.listBoxConsole, 1, 3);
            this.tableLayoutPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelMain.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
            this.tableLayoutPanelMain.RowCount = 4;
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 12F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.Size = new System.Drawing.Size(394, 325);
            this.tableLayoutPanelMain.TabIndex = 0;
            // 
            // labelTimer1
            // 
            this.labelTimer1.AutoSize = true;
            this.labelTimer1.Location = new System.Drawing.Point(3, 0);
            this.labelTimer1.Name = "labelTimer1";
            this.labelTimer1.Padding = new System.Windows.Forms.Padding(0, 8, 0, 0);
            this.labelTimer1.Size = new System.Drawing.Size(39, 21);
            this.labelTimer1.TabIndex = 1;
            this.labelTimer1.Text = "Timer1";
            // 
            // labelTimer2
            // 
            this.labelTimer2.AutoSize = true;
            this.labelTimer2.Location = new System.Drawing.Point(3, 28);
            this.labelTimer2.Name = "labelTimer2";
            this.labelTimer2.Padding = new System.Windows.Forms.Padding(0, 8, 0, 0);
            this.labelTimer2.Size = new System.Drawing.Size(39, 21);
            this.labelTimer2.TabIndex = 2;
            this.labelTimer2.Text = "Timer2";
            // 
            // buttonSetTimer1
            // 
            this.buttonSetTimer1.Location = new System.Drawing.Point(321, 3);
            this.buttonSetTimer1.Name = "buttonSetTimer1";
            this.buttonSetTimer1.Size = new System.Drawing.Size(70, 22);
            this.buttonSetTimer1.TabIndex = 3;
            this.buttonSetTimer1.Text = "Set";
            this.buttonSetTimer1.UseVisualStyleBackColor = true;
            this.buttonSetTimer1.Click += new System.EventHandler(this.buttonSetTimer1_Click);
            // 
            // buttonSetTimer2
            // 
            this.buttonSetTimer2.Location = new System.Drawing.Point(321, 31);
            this.buttonSetTimer2.Name = "buttonSetTimer2";
            this.buttonSetTimer2.Size = new System.Drawing.Size(70, 22);
            this.buttonSetTimer2.TabIndex = 4;
            this.buttonSetTimer2.Text = "Set";
            this.buttonSetTimer2.UseVisualStyleBackColor = true;
            this.buttonSetTimer2.Click += new System.EventHandler(this.buttonSetTimer2_Click);
            // 
            // dateTimePickerTimer1
            // 
            this.dateTimePickerTimer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dateTimePickerTimer1.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateTimePickerTimer1.Location = new System.Drawing.Point(79, 3);
            this.dateTimePickerTimer1.Name = "dateTimePickerTimer1";
            this.dateTimePickerTimer1.Size = new System.Drawing.Size(236, 20);
            this.dateTimePickerTimer1.TabIndex = 5;
            // 
            // dateTimePickerTimer2
            // 
            this.dateTimePickerTimer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dateTimePickerTimer2.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateTimePickerTimer2.Location = new System.Drawing.Point(79, 31);
            this.dateTimePickerTimer2.Name = "dateTimePickerTimer2";
            this.dateTimePickerTimer2.Size = new System.Drawing.Size(236, 20);
            this.dateTimePickerTimer2.TabIndex = 6;
            // 
            // listBoxConsole
            // 
            this.listBoxConsole.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxConsole.FormattingEnabled = true;
            this.listBoxConsole.Location = new System.Drawing.Point(79, 71);
            this.listBoxConsole.Name = "listBoxConsole";
            this.listBoxConsole.Size = new System.Drawing.Size(236, 251);
            this.listBoxConsole.TabIndex = 7;
            // 
            // TimerTestFormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(394, 325);
            this.Controls.Add(this.tableLayoutPanelMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "TimerTestFormMain";
            this.Text = "Timer Test";
            this.tableLayoutPanelMain.ResumeLayout(false);
            this.tableLayoutPanelMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain;
        private System.Windows.Forms.Label labelTimer1;
        private System.Windows.Forms.Label labelTimer2;
        private System.Windows.Forms.Button buttonSetTimer1;
        private System.Windows.Forms.Button buttonSetTimer2;
        private System.Windows.Forms.DateTimePicker dateTimePickerTimer1;
        private System.Windows.Forms.DateTimePicker dateTimePickerTimer2;
        private System.Windows.Forms.ListBox listBoxConsole;
    }
}

