﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TimerTest
{
    public partial class TimerTestFormMain : Form
    {
        delegate void appendToConsoleCallback(string text);

        private TimerTester timerTester1_;
        private TimerTester timerTester2_;

        public TimerTestFormMain()
        {
            timerTester1_ = new TimerTester();
            timerTester2_ = new TimerTester();
            InitializeComponent();
        }

        public void appendToConsole(string text)
        {
            if (listBoxConsole.InvokeRequired)
            {
                appendToConsoleCallback a = new appendToConsoleCallback(appendToConsole);
                this.Invoke(a, new object[] { text });
            }
            else
            {
                this.listBoxConsole.Items.Add(text);
            }
        }

        private void buttonSetTimer1_Click(object sender, EventArgs e)
        {
            timerTester1_.setTimer(dateTimePickerTimer1.Value, this);
        }

        private void buttonSetTimer2_Click(object sender, EventArgs e)
        {
            timerTester2_.setTimer(dateTimePickerTimer2.Value, this);
        }
    }
}
