﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TimerTest
{
    class TimerTester
    {
        private Timer timer_;

        public void setTimer(DateTime eventDateTime, TimerTestFormMain formMain)
        {
            timer_ = new Timer(updateTextBoxOnTimer, formMain, (Int64)eventDateTime.TimeOfDay.Subtract(DateTime.Now.TimeOfDay).TotalMilliseconds, 0);
            formMain.appendToConsole("Timer registered in " + eventDateTime.TimeOfDay.Subtract(DateTime.Now.TimeOfDay).TotalMilliseconds.ToString() + "ms");
            formMain.appendToConsole("Timer registered @ " + (DateTime.Now + eventDateTime.TimeOfDay.Subtract(DateTime.Now.TimeOfDay)).ToLongTimeString());
        }

        private void updateTextBoxOnTimer(object state)
        {
            TimerTestFormMain form = (TimerTestFormMain)state;
            form.appendToConsole("Timer called @ " + DateTime.Now.ToLongTimeString());
        }
    }
}
